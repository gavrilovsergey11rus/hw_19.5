#include <iostream>
#include <random>
#include <ctime>

class Animal
{
public:
   virtual void Voice()
    {
        std::cout << "Default voice" << std::endl;
    }

   virtual ~Animal(){}
};

class Dog : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Woof" << std::endl;
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Meow" << std::endl;
    }
};

class Mouse : public Animal
{
public:
    void Voice() override
    {
        std::cout << "3.14" << std::endl;
    }
};

int main()
{
    const int size = 10;
    Animal* pets[size];
    int a;

    std::mt19937 gen(time(0));
    std::uniform_int_distribution<>uid(1, 3); //������������

    for (int i = 0; i < size; i++)
    {
        a = uid(gen);

        switch (a)
        {
            case 1:
                pets[i] = new Dog;
                continue;
            case 2:
                pets[i] = new Cat;
                continue;
            case 3:
                pets[i] = new Mouse;
                continue;
        }
    }

    std::cout << "Animals:" << '\n' << std::endl;

    for (int i = 0; i < size; i++)
    {

        pets[i]->Voice();
        delete pets[i];
    }
        

    return 0;
}